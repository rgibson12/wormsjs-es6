import InputController from 'objects/InputController';
import ExplosionController from 'objects/ExplosionController';

//abstract class for weapons to extend
class Weapon extends Phaser.Sprite {

  constructor(game, x, y, sprite, explosionPower, ammoCapacity, fuseTime, explosionRadius, range, hasTaget, precision, maxDmg) {
    super(game, x, y, sprite);
    this.game = game;
    this.y=y;
    this.x=x;
    this.sprite=sprite;
    this.explosionPower = explosionPower;
    this.ammoCapacity = ammoCapacity;
    this.fuseTime = fuseTime;
    this.explosionRadius = explosionRadius;
    this.range = range;
    this.hasTaget = hasTaget;
    this.charging = false;
    this.precision = precision;
    this.explosionController = new ExplosionController(this.game);
    this.game.stage.addChild(this);
    this.anchor.setTo(0,0.5);

    //refactor to own class
    this.turretPointerWidth = 90;
    this.turretPointer = new Phaser.Line(0,0,0,0);

    //refactor to own class
    this.powerIndicator=[];
    this.powerRedIntensity=122;
    this.currentPowerIndex=0;
    this.powerIndicatorRadius=0;

    this.maxDmg=maxDmg;
	}

  static get FIREPOWER(){
    return 1500;
  }

  //** stub overridden by all subsequent child classes. **
  fire(physicsBodiesPaths, physicsBodies, groundBody){

  }

  getType(){
    return this.constructor.name;
  }

  getBullet(){
    if (this.bullet){
      return this.bullet;
    } else {
      return false;
    }
  }

  getFiringRedCol(){
    return this.powerRedIntensity;
  }

  isReloading(){
    return false;
  }

  getWidth(){
    return this.width;
  }

  getPower(){
    return this.explosionPower;
  }

  getTurretPointer(){
    return this.turretPointer;
  }

  getPowerIndicator(){
    return this.powerIndicator;
  }

  charge(firingPower){
    //console.log("CHARGING AT SUPER");
    //if there are shots left in the "clip"
      this.charging=true;

      //if not fully charged
      if (firingPower < Weapon.FIREPOWER){
        firingPower+= Weapon.FIREPOWER/100;

        //coordinates of powerIndicatorMarker
        var pointerCoords = [];
        this.turretPointer.coordinatesOnLine(1, pointerCoords);

        //if charging has not begun, begin it
        if (this.powerIndicator[0] == undefined){
          //explosionPower indicator is a series of drawn circles, increasing in size, along the turretPointerMarker
          this.powerIndicator.push(new Phaser.Circle(pointerCoords[0].x, pointerCoords[0].y, 5));
          this.powerIndicatorRadius = 5;
        //if already charging, continue
        } else {
          this.powerIndicatorRadius+=0.2;
          if(this.currentPowerIndex < pointerCoords.length){
            this.powerIndicator.push(new Phaser.Circle(pointerCoords[this.currentPowerIndex][0] ,pointerCoords[this.currentPowerIndex][1], this.powerIndicatorRadius));
          }
        }
        //increase intensity/brightness of powerIndicator colouring in proportion with increase in firepower
        this.powerRedIntensity += 0.5+(0.1*this.currentPowerIndex);
        this.currentPowerIndex++;
      }
    return firingPower;
  }

  resetCharge(){
    this.charging = false;
    //clear powerIndicator
    this.powerIndicator.splice(0, this.powerIndicator.length);
    this.currentPowerIndex=0;
  }

  aimLeft(){
    this.angle-=2;
  }

  aimRight(){
    this.angle+=2;
  }

  holster(){
    this.visible = false;
  }

  draw(){
    this.visible = true;
  }

  isCharging(){
    return this.charging;
  }


  /********************************************************************
  *                      PHASER SPRITE METHODS                        *
  *********************************************************************/

  update(){
    //console.log("BAZOOKA updating");
    var p = new Phaser.Point(this.x,this.y);
    p.rotate(p.x, p.y, this.rotation,false,this.getWidth());
    this.turretPointer.fromAngle(p.x,p.y,this.rotation, this.turretPointerWidth);

    //console.log(this.turretPointer.x1+ " and "+ (this.x+this.getWidth()));
  }


}

export default Weapon;
