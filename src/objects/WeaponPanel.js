class WeaponPanel extends Phaser.Sprite {

	constructor(game, x, y, sprite) {
		super(game, x, y, sprite);
		this.game=game;
		this.x = x;
		this.y=y;
		this.cellWidth=28;
		this.weapons=[];
	}

	setWeapons(wpns){
		this.weapons = wpns;
	}

	changeTo(){
		this.game.stage.addChild(this);
		//this.bringToTop();
	}

	changeFrom(){
		this.game.stage.removeChild(this);
	}


	disable(){
		this.loadTexture('weaponPanel_disabled');
		//this.bringToTop();
	}

	enable(){
		this.loadTexture(this.curSprite);
		//this.bringToTop();
	}

	getWidth(){
		return this.width;
	}

	getHeight(){
		return this.height;
	}

	getCellWidth(){
		return this.cellWidth;
	}

	resolveClick(x, y){
		var click = Math.floor(x/this.cellWidth);

		//first weapon cell starts at 28px. 28->55/28 ~= 1, but index starts at 0. deduct 1 from calculated weapon index
		click-=1;

		return this.weapons[click];
	}



}

export default WeaponPanel;
