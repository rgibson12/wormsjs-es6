import Weapon from 'objects/Weapon';
import ExplosionController from 'objects/ExplosionController';

class Bullet extends Phaser.Sprite {

  constructor(game, x, y, sprite, explosionRadius, fuse, precision, gravScale, explosionPower, maxDmg) {
    super(game, x, y, sprite);

    this.game = game;
    this.x = x;
    this.y = y;
    this.gravityScale = gravScale;
    //explosion specific properties from parent weapon
    this.explosionRadius = explosionRadius;
    this.fuse = fuse;
    this.precision = precision;
    this.explosionPower = explosionPower;
    this.maxDmg = maxDmg;
    //explosion controller
    this.explosionCtrl = new ExplosionController(this.game);
    this.detonating=false;
	}

  fireBullet(wpnRotation, firePower, wpnLength, physicsBodiesPaths, physicsBodies, groundBody){
    //create bullet physics body at tip of weapon barrel, apply angular velocity based on angle of fire (weapon rotation)
    var p = new Phaser.Point(this.x, this.y);
    p.rotate(p.x, p.y, wpnRotation, false, wpnLength);
    this.x = p.x;
    this.y = p.y;
    this.game.stage.addChild(this);
    this.game.physics.box2d.enable(this);
    this.body.setCircle(3);
    this.body.rotation = wpnRotation;
    this.body.gravityScale = this.gravityScale;
    //calculate velocity of bullet, factoring weapon angle and fire power
    p.setTo((Math.cos(wpnRotation) *firePower), (Math.sin(wpnRotation) *firePower));
    //refactor to negative velocty once facing-direction implemented
    //if(context.activeWorm.getFacingRight()){
      this.body.velocity.x = p.x;
      this.body.velocity.y = p.y;
    //}
    //} else {
      //this.bullet.body.velocity.x = -p.x;
      //this.bullet.body.velocity.y = -p.y;
    //}

    //Explosion specific data needed by ExplosionController
    //In JSON format as scope of 'this' is lost between callbacks due to function scoping
    var explodeData = {
      me: this,
      game: this.game,
      physicsBodies: physicsBodies,
      physicsBodiesPaths: physicsBodiesPaths,
      groundBody: groundBody,
      firePower: firePower
    }

    //if fuse given, trigger explosion on fuse-delay
    if(this.fuse > 0){
      console.log("USING FUZE TO EXPLODE");
      this.fuseTimer = this.game.time.create(this.game, true);
      this.fuseTimer.add(Phaser.Timer.SECOND * this.fuse, this.explodeBullet, explodeData);
      this.fuseTimer.start();
    } else {
    //if no fuse given, trigger explosion on collision with worm bodies (3) and ground bodies (2)
    console.log("EXPLODING IMMEDIATELY");
    this.explodeData = explodeData;
      this.body.setCategoryContactCallback(2, this.collideBullet, this);
      this.body.setCategoryContactCallback(3, this.collideBullet, this);
    }

  }

  collideBullet(){
    //context variables from received JSON
    if (!this.detonating){
      this.detonating = true;
      //var explodeData = this, me = explodeData.me, game=explodeData.game;
      this.body.removeFromWorld();
      console.log("COLLIDING BULLET");
      //trigger explosion callback after weapon fuse countdown
      this.fuseTimer = this.game.time.create(this.game, true);
      this.fuseTimer.add(0, this.explodeBullet, this.explodeData);
      this.fuseTimer.start();
    }
  }

  explodeBullet(){
    //context variables from received JSON
    console.log("creating explosion from bullet class");
    var explodeData = this, me = explodeData.me;

    me.destroy();
    //create explosion at bullet location, of size/precision dictated by firing weapon properties
    me.explosionCtrl.createExplosion(new Phaser.Point(me.x,me.y), me.explosionRadius, me.precision, this.physicsBodiesPaths, this.physicsBodies, this.groundBody, me.explosionPower, me.maxDmg, this.firePower);
    this.detonating=false;
  }


  update(){
    //if bullet exceeds bounds of 1.5x screen size, kill it
    if (this.x > $(window).width()*1.5 || this.y > $(window).height() * 1.5){
      this.kill();
      this.destroy();
      console.log("bullet out of bounds - killed");
    }
  }

}

export default Bullet;
