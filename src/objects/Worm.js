import Weapon from 'objects/Weapon';
import Bazooka from 'objects/Bazooka';
import Multishot from 'objects/Multishot';
import Arsenal from 'objects/Arsenal';

class Worm extends Phaser.Sprite{

  constructor(game, x, y, sprite, id, teamArsenal, teamCol) {
    super(game, x, y, sprite);
    this.game = game;
    this.y=y;
    this.x=x;
    this.sprite=sprite;
    this.moveSpeed=60;
    this.weaponDrawn = false;
    this.game.physics.enable([this], Phaser.Physics.BOX2D);
    this.body.collisionType="wormType";
    this.body.fixedRotation=true;
    this.body.setCollisionCategory(3);
		this.game.stage.addChild(this);
    this.id=id;

    //aresnel received from team
    this.teamArsenal = teamArsenal;
    //start with bazooka - inf. supply
    this.activeWeapon = this.teamArsenal.get('Bazooka', this.x, this.y);
    this.holsterWeapon();

    this.alive=true;
    this.hp=100;
    this.hpLabel = game.add.text(this.x, this.y - 20, this.hp, { fill: teamCol, font: '8pt Arial',stroke: 'rgba(255,255,255,1)'});
		this.hpLabel.anchor.setTo(0.5,0.5);
	}

  //player update loop
  update(){
    //position selected weapon on player character
    if(this.weaponDrawn){
      this.activeWeapon.x = this.x;
      this.activeWeapon.y = this.y;
    }
    this.hpLabel.x = this.x;
    this.hpLabel.y=this.y-20;
  }

  getX(){
    return this.x;
  }

  getY(){
    return this.y;
  }

  applyDamage(dmg){
    if(this.hp < dmg){
      this.hp=0;
      //KILL WORM
    } else {
      this.hp-=dmg;
    }
    this.updateHpLabel();
  }

  updateHpLabel(){
    this.hpLabel.setText(this.hp);
  }

  changeWpn(wpnKey){
    this.holsterWeapon();

    let changedWpn = this.teamArsenal.get(wpnKey, this.x, this.y);
    //swap weapon, if can be swapped (arsenal has supply left)
    if(changedWpn){
      this.activeWeapon = changedWpn;
    }
    this.drawWeapon();
  }

  consumeWeapon(wpn){
    this.teamArsenal.use(wpn);
  }

  //alive state of worm
  isAlive(){
    return this.alive;
  }

  //rotate weapon aim updwards
  aimWeaponLeft(){
    this.drawWeapon();
    this.activeWeapon.aimLeft();
  }

  //rotate weapon aim downwards
  aimWeaponRight(){
    this.drawWeapon();
    this.activeWeapon.aimRight();
  }

  //show weapon sprite
  drawWeapon(){
    this.activeWeapon.draw();
    this.weaponDrawn = true;
  }

  //hide weapon sprite
  holsterWeapon(){
    this.activeWeapon.holster();
    this.weaponDrawn = false;
  }

  //charge weapon before firing
  chargeWeapon(power){
    var newPower = this.activeWeapon.charge(power);
    return newPower;
  }

  //fire weapon - pass physics bodies for explosion processing
  fireWeapon(physicsBodiesPaths, physicsBodies, groundBody, firePower, gamestate){
    //console.log(gamestate);
    this.activeWeapon.fire(physicsBodiesPaths, physicsBodies, groundBody, firePower, gamestate);
    //consume 1 supply of weapon in team's arsenal
    this.consumeWeapon(this.activeWeapon);
  }

  //current selected weapon
  getActiveWeapon(){
    return this.activeWeapon;
  }

  //direcitonal movement
  move(dir){
    this.holsterWeapon();
    if(dir=='l'){
      this.body.moveRight(this.moveSpeed*-1);
    } else if (dir=='r'){
      this.body.moveRight(this.moveSpeed);
    }
  }

}

export default Worm;
