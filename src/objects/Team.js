import Worm from 'objects/Worm';
import InputController from 'objects/InputController';
import Arsenal from 'objects/Arsenal';

class Team {

  constructor(game, teamColour, id) {
    this.game = game;
    this.id=id;

    //======= FINISH IMPLEMENTATION ==============
    this.arsenal = new Arsenal(this.game);

    //colour used for text rendering
    this.colour = teamColour;
    //number of worms per team
    this.teamSize = 1;
    //index of last worm played in team
    this.playingMarker = -1;
    //team worms collection
    this.worms=[];
    //populate team with worms

    this.create();
	}

  create(){
    for(var i=0; i< this.teamSize; i++){
    	let newWorm = new Worm(this.game, /*Math.floor((Math.random() * 250)+1)*/500, 250, 'tank',i, this.arsenal, this.colour);
      this.worms.push(newWorm);
    }
  }

  getColour(){
    return this.colour;
  }

  startWith(){
    //first turn taken by this teams first worm - set playingMarker index to first worm index
    this.playingMarker=0;
  }

  getAllWorms(){
    return this.worms;
  }

  getWorm(index){
    return this.worms[index];
  }

  incrementPlayer(){
    this.playingMarker++;
  }

  nextWorm(nextPlayerMarker){
    //if not at end of team, get next worm
    if ((nextPlayerMarker) < this.worms.length){
      var nextWorm = this.worms[nextPlayerMarker];

      //if next worm is dead, recursively get worm after next worm
      if(!nextWorm.isAlive()){
        this.playingMarker=nextPlayerMarker+1;
        return this.nextWorm(nextPlayerMarker+1);
      } else {
      //if next worm is alive, return for its turn
        this.playingMarker=nextPlayerMarker;
        console.log("Giving turn to worm: " + nextPlayerMarker +", of team: " + this.id);
        return nextWorm;
      }
    } else {
    //if at end of team, recursively get first worm
      this.playingMarker=0;
      return this.nextWorm(0);
    }
  }

  currentPlayerMarker(){
    return this.playingMarker;
  }

  currentWorm(){
    return this.getWorm(this.playingMarker);
  }

}

export default Team;
