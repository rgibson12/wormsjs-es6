import Weapon from 'objects/Weapon';
import Bazooka from 'objects/Bazooka';
import Grenade from 'objects/Grenade';
import Multishot from 'objects/Multishot';

class Aresnal {

	constructor(game) {
		this.game = game;
		this.weapons = {};
		this.populate();
	}

	populate(){
		//initial supplies for all weapons
		this.weapons.Bazooka = 500000;
		this.weapons.Multishot = 5;
		this.weapons.Grenade = 2;
	}

	add(weaponKey){
		//add +1 supply to given weapon in arsenal
		this.weapons[weaponKey]++;
	}

	use(weapon){
		let wpnType = weapon.getType();

		//if weapon is in aresnal
		if(this.weapons[wpnType]){
			//use 1 supply of given weapon in arsenal
			this.weapons[wpnType]--;
		}
	}

	set(weaponKey, amount){
		//set supply for given weapon as given value in arsenal
		this.weapons[weaponKey]=amount;
	}

	get(weaponKey, x, y){
		//if supply for new weapon hasn't been depleted
		if(this.weapons[weaponKey] > 0){
			switch (weaponKey) {
				case 'Bazooka':
					return new Bazooka(this.game, x,y);
					break;

				case 'Multishot':
					return new Multishot(this.game, x,y);
					break;

				case 'Grenade':
					return new Grenade(this.game, x,y);
					break;

				default:
					return null;
					break;
			}
		} else {
			console.log("OUT OF USES FOR " + weaponKey)
			return null;
		}

	}

}

export default Aresnal;
