import WeaponPanel from 'objects/WeaponPanel';

class WeaponPanelController {

	constructor(game) {
		this.game=game;
		//collection for all weapon panels in game
		this.panels= [];

		//firs weapon panel
		this.panels.push(new  WeaponPanel(this.game, 0, 0, 'weaponPanel1'));
		this.panels[0].setWeapons(['Bazooka', 'Grenade', 'Multishot', '','','','','']);

		//2nd weapon panel
		this.panels.push(new  WeaponPanel(this.game, 0, 0, 'weaponPanel2'));
		this.panels[1].setWeapons(['Bazooka', 'Grenade', 'Multishot', '','','','','']);

		//start on first weapon panel
		this.currentPanel=this.panels[0];
		this.currentPanel.changeTo();

		this.panelCount=2;
	}

	getWidth(){
		return this.panels[0].getWidth();
	}

	getHeight(){
		return this.panels[0].getHeight();
	}

	getCellWidth(){
		return this.panels[0].getCellWidth();
	}

	//swtich to next panel
	changeFwd(){
		let oldPanelPos = this.panels.indexOf(this.currentPanel);

		this.currentPanel.changeFrom();

		if(oldPanelPos<(this.panels.length-1)){
			this.currentPanel = this.panels[oldPanelPos+1];
		} else {
			this.currentPanel = this.panels[0];
		}

		this.currentPanel.changeTo();
	}

	//switch to previous panel
	changeBckwd(){
		let oldPanelPos = this.panels.indexOf(this.currentPanel);

		this.currentPanel.changeFrom();

		if(oldPanelPos>=(this.panels.length-1)){
			this.currentPanel = this.panels[oldPanelPos-1];
		} else {
			this.currentPanel = this.panels[this.panels.length-1];
		}

		this.currentPanel.changeTo();
	}

	//resolve clicked weapon - to change to
	resolveClick(x, y){
		return this.currentPanel.resolveClick(x,y);
	}

}

export default WeaponPanelController;
