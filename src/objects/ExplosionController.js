import Weapon from 'objects/Weapon';

class ExplosionController {

  constructor(game) {
    this.game = game;

	}


  createExplosion(center, radius, precision, bodyPaths, bodies, groundBody, explosionPower, maxDmg, firePower){
    //explosion circle for rendering in debug mode
    var explosion = new Phaser.Circle(center.x, center.y, radius);
    console.log(maxDmg);
    //circle path for clipper - number of verts = weapon precision
    var circlePath = this.createCircleOfVerts(precision, center, radius/2);

    //subjectPaths for clipper - intpoints of all coord sets in physicsBodiesPaths
    var subj_paths = new ClipperLib.Paths();
    for(var x = 0; x < bodyPaths.length; x++){
			var subj_path = new ClipperLib.Path();
			var toPush = bodyPaths[x];
			for (var i = 0; i< toPush.length; i+=2){
				subj_path.push(
					new ClipperLib.IntPoint(toPush[i], toPush[i+1]));
			}
			subj_paths.push(subj_path);
			subj_path = null;
		}

    //assemble to-clip paths of clipper intpoints from points on explosion circle
    var clip_paths = new ClipperLib.Paths();
		var clip_path = new ClipperLib.Path();
		for(i=0; i < circlePath.length; i+=2){
			clip_path.push(
                                            //x                         y
				new ClipperLib.IntPoint(Math.round(circlePath[i]), Math.round(circlePath[i+1])));
		}
    clip_paths.push(clip_path);

    //scale paths
		var scale = 1;
		ClipperLib.JS.ScaleUpPaths(subj_paths, scale);
		ClipperLib.JS.ScaleUpPaths(clip_paths, scale);

    //run clip operation to produce clipped paths
    var cpr = new ClipperLib.Clipper();
		cpr.AddPaths(subj_paths, ClipperLib.PolyType.ptSubject, true);
		cpr.AddPaths(clip_paths, ClipperLib.PolyType.ptClip, true);
    var solution_paths =[];
		var clipType = ClipperLib.ClipType.ctDifference;
		var subject_fillType = ClipperLib.PolyFillType.pftNonZero;
		var clip_fillType = ClipperLib.PolyFillType.pftNonZero;
		var succeeded = cpr.Execute(clipType, solution_paths, subject_fillType, clip_fillType);

    //set game bodies, goundBody and bodyPaths to clipped paths/bodies
    var newBods = [];
		var newPaths = [];
		var toX, toY;

    //clear game ground body
		groundBody.clearFixtures();

    //produce clipped ground body path
		for (i = 0; i < solution_paths.length; i++){
			var newGroundPath = [];
			var poly = solution_paths[i];
			for(var j = 0; j < poly.length; j++){
				var vert = poly[j];
				if (j == 0){
					toX = vert.X;
					toY = vert.Y;
				}
				newGroundPath.push(vert.X);
				newGroundPath.push(vert.Y);
	    }
			newGroundPath.push(toX);
			newGroundPath.push(toY);

      //add clipped gound chain body, from clipped ground body paths, to game ground body
      //push to new bodies and new paths collections
			newBods.push(groundBody.addChain(newGroundPath));
			newPaths.push(newGroundPath);
		}

    //empty game bodies and bodyPaths collections
		bodies.splice(0, bodies.length);
		bodyPaths.splice(0, bodyPaths.length);

    //set game bodies and body paths as clipped bodies and body paths
	  for (i = 0; i < newBods.length; i++){
			bodies.push(newBods[i]);
		}
		for (i = 0; i < newPaths.length; i++){
			bodyPaths.push(newPaths[i]);
	  }

    //collide game ground body with worms and bullets
		groundBody.setCollisionCategory(2);

    this.effectWorms(this.game, explosion, explosionPower, maxDmg, firePower)
  }

  createCircleOfVerts (precision, origin, radius) {
    //create "circle" of connected vertices - number of verts = precision = roundness of circle
		var angle=2*Math.PI/precision;
		var circleArray = [];
		var multiplier = 0;
		for (var i=0; i<(precision); i++) {
			multiplier = i*2;
			circleArray[multiplier] =(origin.x+radius*Math.cos(angle*i));
			circleArray[multiplier+1] = (origin.y+radius*Math.sin(angle*i));
		}
		return circleArray;
	}


  effectWorms(game, explosion, explosionPower, maxDmg, firePower){
    let teams = game.turnSystem.getTeams();

    //process all worms of both teams
    for (var i = 0; i < teams.length; i++){
      let aTeam = teams[i].getAllWorms();
      for (var j = 0; j < aTeam.length; j++){
        let aWorm = aTeam[j];
        if (aWorm.isAlive()){
          let wormBounds = aWorm.getBounds(), wormX=aWorm.getX(), wormY=aWorm.getY();
          //if worm is within explosion radius
          if (Phaser.Circle.intersectsRectangle(explosion, wormBounds)){
            //calculate angular x velocity to apply to worm
            var vx = this.angularVelocity(
                      ((explosionPower/explosion.radius)),
                      explosion.x-wormX,
                      explosionPower
                    ),
            //calculate angular y velocity to apply to worm
                vy = this.angularVelocity(
                      ((explosionPower/explosion.radius)),
                      explosion.y-wormY,
                      explosionPower
                    );

            /*====== USE TO APPLY DMG/VELOCITIES BASED ON CHARGE OF WEAPON SHOT ALSO ====
            var pwrFactor = firePower/Weapon.FIREPOWER;
            ============================================================================*/

            //apply velocities to worm
            aWorm.body.velocity.x = vx;
            aWorm.body.velocity.y = vy;
            //apply damage to worm
            this.damageWorm(1, explosion, aWorm, maxDmg);
          }
        }
      }
    }
  }


  damageWorm(multiplier,explosion, aWorm, maxDmg){
    var dist = Math.abs(explosion.distance(new Phaser.Point(aWorm.getX(),aWorm.getY())));
    if(dist<1){
      dist=1;
    }
    console.log("explosion radius: " + explosion.radius + ". dst: " + dist);
    var dmgFactor = (1 - ((explosion.radius - dist)/explosion.radius));
    var dmg =maxDmg - (Math.round(maxDmg * dmgFactor));
    if (dmg>maxDmg){
      dmg=maxDmg;
    }
    console.log("Max Damgae: " + maxDmg + ". Applying damage: " + dmg*multiplier + ".... dmg factor: " + dmgFactor);
    aWorm.applyDamage(dmg*multiplier);
  }


  angularVelocity(m, x, c){
    /*velocity = linearly decreasing, product of distance from explosion
    velocty/distance
    max value = weapons explosionPower
    Y = -m(x) + c calculates iterception point on a the graph of velocity/distance == velocity to apply */
    var velocity = ((m*-1)*x)+c;
    if(x>0){
      velocity*=-1;
    }

    return velocity;
  }
}

export default ExplosionController;
