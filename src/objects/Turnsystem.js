import Worm from 'objects/Worm';
import Team from 'objects/Team';
import InputController from 'objects/InputController';

//instance class for bazooka weapon
class TurnSystem {

  constructor(game, inputCtrl) {
    this.game=game;
    this.inputCtrl = inputCtrl;

    //teams
    this.teams=[];
    this.teams.push(new Team(this.game, 'red', 1));
    this.teams.push(new Team(this.game, 'blue', 2));

    //team being played
    this.activeTeam=this.teams[0];

    //turnTime
    this.turnTime = 10;
    //turn-change timer
    this.nextTurnTimer=null;
    this.endTurnTimer=null;

	}


  //start game and timers
  start(gameState){
    //set first playing worm
    gameState.changePlayer(this.activeTeam.getWorm(0));

    //set active worm index for starting team
    this.activeTeam.startWith();

    //start turn timer
    this.startTurnTimer(gameState);
  }

  //start timer for a single turn
  startTurnTimer(gameState, turnTime){
    //kill previous turns timer
    if (this.nextTurnTimer){
      this.nextTurnTimer.destroy();
    }
    if (this.endTurnTimer){
      this.endTurnTimer.destroy();
    }

    if(turnTime==undefined){
      turnTime = this.turnTime;
    }
    //for callback
    var nextTurnData = {
      me: this,
      gameState: gameState
    };

    //create new timer to trigger next turn in 60s
    this.nextTurnTimer = this.game.time.create();
    this.nextTurnTimerEvent = this.nextTurnTimer.add(Phaser.Timer.SECOND * turnTime, this.startEndTurnTimer, nextTurnData);
    //begin turn
    this.nextTurnTimer.start();
  }

  getTeams(){
    return this.teams;
  }

  //change turn, triggered by turnTimer
  nextTurn(){
    let me = this.me;
    let gameState = this.gameState;

    //change teams
    if(me.activeTeam==me.teams[0]){
      me.activeTeam = me.teams[1];
    } else {
      me.activeTeam = me.teams[0];
    }

    //get next worm from TurnSystem, set as games active worm
    gameState.changePlayer(
      me.getNextWorm(me.activeTeam)
    );

    //start next turns timer
    me.startTurnTimer(gameState);
    me.inputCtrl.toggleWeaponControlls(true);
  }

  //start end-of-turn grace timer - 3s of movement permitted after turn taken
  startEndTurnTimer(){
    let me = this.me;
    let gameState = this.gameState;


    me.endTurnTimer = me.game.time.create();
    me.endTurnTimerEvent = me.endTurnTimer.add(Phaser.Timer.SECOND * 3, me.nextTurn, this);
    //begin turn
    me.endTurnTimer.start();
    me.activeTeam.currentWorm().holsterWeapon();
    me.inputCtrl.toggleWeaponControlls(false);
  }

  //return worm for next turn
  getNextWorm(team){
    //get index of next worm in team
    let wormIndex = team.currentPlayerMarker() +1;
    //get worm at next index in team
    var newWorm = team.nextWorm(wormIndex);
    return newWorm;
  }

  //return duration left on turnTimer
  getTurnTimer(){
    if(this.nextTurnTimer){
      return this.formatTime(Math.round((this.nextTurnTimerEvent.delay - this.nextTurnTimer.ms) / 1000));
    } else {
      return null;
    }
  }

  //return duration left on end-of-turn grace timer
  getEndTurnTimer(){
    if(this.endTurnTimer){
      return this.formatTime(Math.round((this.endTurnTimerEvent.delay - this.endTurnTimer.ms) / 1000));
    } else {
      return null;
    }
  }

  //return colour of active team
  getTeamColour(){
    return this.activeTeam.getColour();
  }

  //format duration of a timer in seconds
  formatTime(s) {
		// Convert seconds (s) to a nicely formatted and padded time string
		var minutes = 0;
		var seconds = "0" + (s - minutes * 60);
		return seconds.substr(-2);
	}


}

export default TurnSystem;
