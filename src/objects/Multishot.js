import Weapon from 'objects/Weapon';
import Bullet from 'objects/Bullet';
import ExplosionController from 'objects/ExplosionController';

//instance class for multishot weapon
class Multishot extends Weapon {

  constructor(game, x, y) {
    super(game, x, y, 'shotgun', 50, 2, 0, 20, null, false, 20, 25);

    this.reloading = false;
    this.reloadingTimer = 2;
    this.shotCount = 0;
    this.firePower = 600;
	}

  fire(physicsBodiesPaths, physicsBodies, groundBody, firePower, gameState){
    //fire as many bullets as weapon supports
    if(!this.reloading){
      if(this.shotCount < this.ammoCapacity){
        //fire bullet;
        this.shotCount++;
        this.bullet = new Bullet(this.game, this.x, this.y, 'bazookaBullet', this.explosionRadius, this.fuseTime, this.precision, 0, this.explosionPower, this.maxDmg);
        this.bullet.fireBullet(this.rotation, this.firePower, this.width, physicsBodiesPaths, physicsBodies, groundBody);
        this.resetCharge();
        this.reload();
        if(this.shotCount == this.ammoCapacity){
          gameState.turnSystem.startTurnTimer(gameState, 0);
        }
      }
    }

    //change turn

  }

  charge(firePower){

  }

  isReloading(){
    return this.reloading;
  }

  reload(){
    this.reloading = true;

    var reloadTimer = this.game.time.create();

    var reloadData = {
      me: this,
      timer: reloadTimer
    }

    var reloadFinish = reloadTimer.add(Phaser.Timer.SECOND * this.reloadingTimer, this.finishReload, reloadData);
    console.log("== reloadING ==");
    //console.log(reloadData.me);
    reloadTimer.start();
  }

  finishReload(){
    var timer = this.timer;
    var me = this.me;

    timer.stop();
    //console.log(me);
    console.log("== reloadED ==");
    me.reloading = false;
  }




  /********************************************************************
  *                      PHASER SPRITE METHODS                        *
  *********************************************************************/
  update(){
    super.update();
  }

}

export default Multishot;
