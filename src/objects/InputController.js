class InputController extends Phaser.State{

  constructor(gameState) {
    super();
    //reference vars
    this.gameState = gameState;
    this.game = this.gameState.game;
    this.subjectWorm = this.gameState.activeWorm;

    //fireBtn enum
    //weapon firing triggered by key-up events. key-up events fire whenever the key is NOT despressed
    //enum prevents weapon firing being triggered unless the key up event follows a key down event
    this.FIREBTNENUM = Object.freeze({
      DOWN: Symbol("FIREBTNENUM.DOWN"),
      INACTIVE: Symbol("FIREBTNENUM.INACTIVE"),
      UP: Symbol("FIREBTNENUM.UP")
    });
    this.fireState = this.FIREBTNENUM.INACTIVE;
    this.firePower=0;
    //controllable bullet controls
    this.cursorKeys=this.game.input.keyboard.createCursorKeys();

    //movement controls
	  this.moveRight = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
  	this.moveLeft = this.game.input.keyboard.addKey(Phaser.Keyboard.A);

    //aiming controls
    this.aimUp = this.game.input.keyboard.addKey(Phaser.Keyboard.W);
  	this.aimDown = this.game.input.keyboard.addKey(Phaser.Keyboard.S);

    //fire control
    this.fireBtn = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    //this.fireBtn.onUp.add(this.subjectWorm.fireWeapon(this.gameState.physicsBodiesPaths, this.gameState.physicsBodies, this.gameState.groundBody), this);

    //selection controlls
    this.lClick = this.game.input.onDown.add(this.resolveClick, this);

	}

  resolveClick(event){
    //weapon panel coordinates, not including change arrows
    let panelWidth = this.gameState.weaponPanel.getWidth(),
        x1=this.gameState.weaponPanel.getCellWidth(),
        x2= panelWidth - this.gameState.weaponPanel.getCellWidth(),
        y1=0,
        y2=this.gameState.weaponPanel.getHeight();

    //if click is on a weapon icon
    if( (y1 < event.y && event.y < y2)){
  		if ((x1 < event.x && event.x <x2)){
        //resolve clicked icon to corresponding weapon
        var wpnChange = this.gameState.weaponPanel.resolveClick(event.x, event.y);
        //change to clicked weapon
        this.subjectWorm.changeWpn(wpnChange);
      } else if ((x1 > event.x)) {
        //next weapon panel click
        this.gameState.weaponPanel.changeBckwd();
      } else if ((x2 < event.x) && (event.x < panelWidth)){
        this.gameState.weaponPanel.changeFwd();
      }
    }

  }

  resetWpnChange(){
    this.me.canChange=true;
  }

  toggleWeaponControlls(onOff){
    this.fireBtn.enabled = onOff;
    this.aimUp.enabled = onOff;
    this.aimDown.enabled = onOff;
    //this.game.input.enabled = onOff;
  }


  update(gameState){
    //refresh gamestate, update worm to currently playing worm
    this.gameState = gameState;
    this.subjectWorm = this.gameState.activeWorm;

    //weapon aim control
    if(this.aimUp.isDown){
			this.subjectWorm.aimWeaponLeft();
		} else if (this.aimDown.isDown){
			this.subjectWorm.aimWeaponRight();
		}//worm movement control
    else if (this.moveLeft.isDown){
      this.subjectWorm.move('l');
		} else if (this.moveRight.isDown){
      this.subjectWorm.move('r');
		}

    //weapon charge-firing control
    //charge on fire button depress
		if (this.fireBtn.isDown){
      //only charge if weapon not reloading
      if(!this.subjectWorm.getActiveWeapon().isReloading()){
        this.firePower = this.subjectWorm.chargeWeapon(this.firePower);
        this.fireState = this.FIREBTNENUM.DOWN;
      }
		} else if (this.fireBtn.isUp){
    //fire on fire button release
      if(!this.subjectWorm.getActiveWeapon().isReloading()){
      //only fire if weapon not reloading
        if(this.fireState==this.FIREBTNENUM.DOWN){
        //only fire if keyUp event follows keyDown event
          //console.log(this.gameState);
          this.subjectWorm.fireWeapon(this.gameState.physicsBodiesPaths, this.gameState.physicsBodies, this.gameState.groundBody, this.firePower, this.gameState);
          this.fireState = this.FIREBTNENUM.INACTIVE;
          this.firePower=0;
        }
      }
    }
  }

}

export default InputController;
