class TextHandle extends Phaser.Text {

	constructor(game, x, y, text) {
		super(game, x, y, text, { font: "12px Arial", fill: "#ff0044", align: "center" });

		this.game.stage.addChild(this);

	}

	startTimer() {
		this.game.time.events.loop(this._speed, this.colorize, this).timer.start();
	}

	colorize() {

		for (let i = 0; i < this.text.length; i++) {

			if (this._colorIndex === this._colors.length) {
				this._colorIndex = 0;
			}

			this.addColor(this._colors[this._colorIndex], i);
			this._colorIndex++;

		}

	}

}

export default TextHandle;
