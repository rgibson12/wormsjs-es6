import Weapon from 'objects/Weapon';
import Bullet from 'objects/Bullet';
import ExplosionController from 'objects/ExplosionController';

//instance class for grenade weapon
class Grenade extends Weapon {

  constructor(game, x, y) {
    super(game, x, y, 'grenade', 200, 1, 5, 100, null, false, 20, 50);

	}

  fire(physicsBodiesPaths, physicsBodies, groundBody, firePower, gameState){
    //fire bullet;
    this.bullet = new Bullet(this.game, this.x, this.y, 'grenade', this.explosionRadius, this.fuseTime, this.precision, 1, this.explosionPower, this.maxDmg);
    this.bullet.fireBullet(this.rotation, firePower, this.width, physicsBodiesPaths, physicsBodies, groundBody);
    this.resetCharge();
    gameState.turnSystem.startTurnTimer(gameState, 0);
    //change turn
  }

  /********************************************************************
  *                      PHASER SPRITE METHODS                        *
  *********************************************************************/
  update(){
    super.update();
  }

}

export default Grenade;
