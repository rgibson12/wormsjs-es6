import Team from 'objects/Team';
import InputController from 'objects/InputController';
import ExplosionController from 'objects/ExplosionController';
import Bazooka from 'objects/Bazooka';
import Multishot from 'objects/Multishot';
import TurnSystem from 'objects/TurnSystem';
import WeaponPanelController from 'objects/WeaponPanelController';


class GameState extends Phaser.State {


	preload(){
		//worm asset
		this.game.load.image("tank", "assets/img/worm_base2.png");
		//weapon assets
		this.game.load.image('bazooka', 'assets/img/bazooka.png');
		this.game.load.image('bazookaBullet', 'assets/img/bullet.png');
		this.game.load.image('shotgun', 'assets/img/shotgun.png');
		this.game.load.image("grenade", "assets/img/grenade2.png");
		//weapon panel assets
		this.game.load.image('weaponPanel1', 'assets/img/weaponPanelBarX.png');
		this.game.load.image('weaponPanel2', 'assets/img/weaponPanelBar2.png');
		this.game.load.image('weaponPanel_disabled','assets/img/weaponPanelBar_disabled.png');
	}

	create() {
		//global vars
		this.physicsBodies = [];
		this.physicsBodiesPaths = [];

		//world
		this.createGameWorld();

		//controls
		this.input = new InputController(this);

		//turnsystem
		this.turnSystem = new TurnSystem(this.game, this.input);
		this.turnSystem.start(this);

		//weaponPanel
		this.weaponPanel = new WeaponPanelController(this.game);

		this.game.turnSystem=this.turnSystem;


	}

	createGameWorld(){
		this.game.stage.backgroundColor = '#000000';
		this.game.world.setBounds(0, 0, 1000, 1000);

		//physics engines and global physics properties
		this.game.physics.startSystem(Phaser.Physics.BOX2D);
		this.game.physics.startSystem(Phaser.Physics.ARCADE);
		this.game.physics.box2d.friction = 0.8;
		this.game.physics.box2d.gravity.y = 500;

		//terrain physics body path REFACTOR INTO IMAGE LOADING + PROCESSING TERRAIN
		this.groundVertices = [100,300,1000,300,1000,1300,100,1300];

		//normalise ground body coordinates
		for (var i = 0; i < this.groundVertices.length; i++){
			this.groundVertices[i] = Math.round(this.groundVertices[i]);
		}

		//create ground body
		this.groundBody = new Phaser.Physics.Box2D.Body(this.game, null, 0, 0, 0);
		this.groundBody.setPolygon(this.groundVertices);
		this.groundBody.setCollisionCategory(2);

		//game physics bodies and body-paths for clipper
		this.physicsBodies.push(this.groundBody);
		this.physicsBodiesPaths.push(this.groundVertices);

	}

	getState(){
		return this;
	}

	changePlayer(newWorm){

		//set new active worm
		this.activeWorm = newWorm;
		this.game.camera.follow(this.activeWorm);

		//reset active weapon to bazooka in case current active weapon is out of uses in arsenal
		this.activeWorm.changeWpn('Bazooka');

		//draw new worms weapon sprite
		this.activeWorm.drawWeapon();
	}




	update(){
		this.input.update(this);
	}

	render(){
		//turret line
		let turrMarker = this.activeWorm.getActiveWeapon().getTurretPointer();
		//timers
		let turnTimer = this.turnSystem.getTurnTimer(), endTurnTimer = this.turnSystem.getEndTurnTimer();
		//weapon charge
		let shotCharge = this.activeWorm.getActiveWeapon().getPowerIndicator(), shotChargeCol = this.activeWorm.getActiveWeapon().getFiringRedCol();

		//draw world + physics bodies
		this.game.debug.box2dWorld();
		//draw turret line
		this.game.debug.geom(turrMarker,'rgba(0,255,0, 0.6)');

		//draw weapon charge graphic at current charge colour
		for (var i=0; i < shotCharge.length; i++){
			this.game.debug.geom(shotCharge[i],'rgba(' + shotChargeCol + ',18,18,1)');
		}

		//output timer countdowns
		this.game.debug.text(turnTimer, $(window).width()-30, 30,this.turnSystem.getTeamColour());
		if(turnTimer <=0){
			this.game.debug.text(endTurnTimer, this.activeWorm.getX()-(this.activeWorm.width/2), this.activeWorm.getY()-30,this.turnSystem.getTeamColour());
		}

	}
}



export default GameState;
