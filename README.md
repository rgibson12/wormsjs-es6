# WormsJS-es6

This is a non-professional recreation of the PlayStation1 game Worms, playable in the browser. I do not have access to the
original game source-code, so this project represents my closest replication of the original game mechanics and functionality (as well as improvements), not a direct port of the source code into JS.

This project serves as a means for teaching myself the new OO features of ES6, and is built upon the [Phaser ES6 boilerplate](https://github.com/belohlavek/phaser-es6-boilerplate) project by codevm.

## Usage

You need [Node.js and npm](https://nodejs.org/). You should also have git installed, but it's not mandatory.

Clone the repository (or download the ZIP file)

`git clone https://rgibson12@bitbucket.org/rgibson12/wormsjs-es6.git`

Install dependencies

`npm install`

Run a development build...

`npm start`

...or a production build.

`npm run production`

Development builds will copy `phaser.min.js` together with `phaser.map` and `phaser.js`
ES6 code is transpiled using Babel into ES5 and concatenated into a single file.

Production builds will only copy `phaser.min.js`. ES6 code will be transpiled using Babel into ES5 and
minified using UglifyJS.


## About me

My name is Richard Gibson: I'm from the UK, and I work full time as a Developer / DevOps engineer. I like to make things in my free time, one day maybe I'll finish something.

## License

This project is released under the MIT License.
